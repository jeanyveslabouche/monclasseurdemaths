!!! abstract "Le ballon va-t-il rentrer dans le panier ?"
    J'ai conçu cette activité d'après un travail de Dan Meyer ([site](http://www.101qs.com/) et [blog](http://blog.mrmeyer.com/) ).

La vidéo suivante montre le début de la trajectoire d'un ballon de basket lors d'une série de 7 tirs. Le but de l'activité est de déterminer si le ballon va rentrer dans le panier dans chacun des cas.
<iframe width="701" height="395" src="https://www.youtube.com/embed/MmHy1rrLL7k" title="Tirs au basket - présentation" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Comme pour tout objet lancé, la trajectoire du ballon est une **parabole**. Elle peut donc être modélisée par la courbe représentative d'**une fonction du second degré** notée $f$.

Cette fonction $f$ , définie sur l'ensemble des réels, est de la forme $f(x)=a(x - α)² + β$. Il s'agit de **la forme canonique** du polynôme du second degré.

1. Montre que $f$ est bien une fonction polynôme du second degré.
1. En utilisant les 3 curseurs du document GeoGebra ci-dessous (Tir n° 1), explique aussi précisément que possible l'influence des trois coefficients $a$, $α$ et $β$ sur l'allure de la courbe (si les documents GeoGebra ne s'affichent pas correctement, tu peux les visualiser ici).
1. Pour le tir n° 1, en faisant varier ces 3 coefficients, détermine si le ballon va rentrer dans le panier ou pas.
1. Détermine de même si le ballon rentrera dans le panier pour les tirs 2 à 5.
1. Pour les tirs 6 et 7, la fonction $f$ est donnée. Trouve une méthode pour déterminer si le ballon va rentrer dans le panier.

## Tir n°1
<iframe scrolling="no" title="Basket - Tir n° 1" src="https://www.geogebra.org/material/iframe/id/CdkF8Sf3/width/700/height/400/border/888888/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="700px" height="400px" style="border:0px;"></iframe>

## Tir n°2
<iframe scrolling="no" title="Basket - Tir n° 2" src="https://www.geogebra.org/material/iframe/id/FWcTuh74/width/700/height/400/border/888888/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="700px" height="400px" style="border:0px;"></iframe>

## Tir n°3
<iframe scrolling="no" title="Basket - Tir n° 3" src="https://www.geogebra.org/material/iframe/id/XBEG9n2y/width/700/height/400/border/888888/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="700px" height="400px" style="border:0px;"></iframe>

## Tir n°4
<iframe scrolling="no" title="Basket - Tir n° 4" src="https://www.geogebra.org/material/iframe/id/EUsZ99yQ/width/700/height/400/border/888888/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="700px" height="400px" style="border:0px;"></iframe>

## Tir n°5
<iframe scrolling="no" title="Basket - Tir n° 5" src="https://www.geogebra.org/material/iframe/id/TmZecwve/width/700/height/400/border/888888/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="700px" height="400px" style="border:0px;"></iframe>

## Tir n°6
<iframe scrolling="no" title="Basket - Tir n° 6" src="https://www.geogebra.org/material/iframe/id/zAr2Huzu/width/700/height/400/border/888888/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="700px" height="400px" style="border:0px;"></iframe>

## Tir n°7
<iframe scrolling="no" title="Basket - Tir n° 7" src="https://www.geogebra.org/material/iframe/id/FgNUhXkB/width/700/height/400/border/888888/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="700px" height="400px" style="border:0px;"></iframe>