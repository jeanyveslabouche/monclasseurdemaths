title: Construire une tangeante à un cercle

!!! abstract "Une vidéo pour expliquer cette construction étape par étape. Attention, on utilise ici seulement une règle non graduée et un compas (pas d'équerre)."

!!! success "La méthode en vidéo"
    |Une explication en vidéo de la construction de la tangente à un cercle en l'un de ses points.|||
    | :------------: | :-------------: | :------------: |
    |<iframe width="560" height="315" src="https://www.youtube.com/embed/hPsuklaUjvE?si=NatHTc2u4Jr1UlCU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|||

