title: Fonctions affines et droites

!!! abstract "Une série de vidéos pour apprendre ou réviser. Une série d'exerciseurs pour s'entraîner."


!!! success "Les vidéos"
    |Sens de variation d'une fonction affine|Sens de variation d'une fonction affine<br>Exemples de démonstrations|Déterminer le signe d'une fonction affine|
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/ndOrKP0mh4U?si=RVE6_8Xo1PADJwEB" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/-iprq9GEcl0?si=2_DjH5UaTXWr-ayB" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/QwBzzQw2bjs?si=7-ulPM1VqNC7gwoA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|

    |Tracer la droite représentative d'une fonction affine|Ordonnée à l'origine d'une droite|Coefficient directeur d'une droite|
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/Tx2WKvKtd0g?si=P2_1wrlQmJTaL9ra" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/VgvBvcG5bC8?si=m4CalIBUuFrEk2pK" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/vVQm4ZvJFaA?si=wUMDS7GBjaeDmcB9" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|

    |Déterminer graphiquement<br> le coefficient directeur d'une droite|Calculer le coefficient directeur d'une droite|Déterminer une fonction affine connaissant<br> deux points de sa droite représentative|
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/0qlhbxmW3pI?si=6lfQ9Aym1tjJ7ySt" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/AeOvx12gDtk?si=0AKxKZOOeAhisDj0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/rG8gupVllyg?si=W_M0qeGjbAj6usiU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|

    |Détrminer l'équation d'une droite en connaissant deux de ses points|||
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/zlfFikFbmks?si=cFR2qH6AF95oBHzw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|||


    !!! success "Les exerciseurs"
        Ils sont répartis dans les six onglets ci-dessous :

        === "Image et<br>antécédent"
            #### Exerciseur 1 : Calculer une image (avec des entiers)
            <iframe scrolling="no" title="101 Calculer une image(avec des entiers)" src="https://www.geogebra.org/material/iframe/id/pgevtvxz/width/800/height/240/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="240px" style="border:0px;"></iframe>
            #### Exerciseur 2 : Calculer une image
            <iframe scrolling="no" title="102 Calculer une image" src="https://www.geogebra.org/material/iframe/id/hfvezumx/width/800/height/310/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="310px" style="border:0px;"></iframe>
            #### Exerciseur 3 : Calculer un antécédent (avec des entiers)
            <iframe scrolling="no" title="103 Calculer un antécédent (avec des entiers)" src="https://www.geogebra.org/material/iframe/id/wcuqss3k/width/800/height/380/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="380px" style="border:0px;"></iframe>
            #### Exerciseur 4 : Calculer un antécédent
            <iframe scrolling="no" title="104 Calculer un antécédent (avec des entiers)" src="https://www.geogebra.org/material/iframe/id/e4kjnfn7/width/800/height/380/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="380px" style="border:0px;"></iframe>

        === "Vocabulaire (nature, coefficient<br>directeur et ordonnée à l'origine)"
            #### Exerciseur 1 : Déterminer la nature d'une fonction avec son expression algébrique
            <iframe scrolling="no" title="201 Déterminer la nature d'une fonction (avec son expression algébrique)" src="https://www.geogebra.org/material/iframe/id/vaqjgnmq/width/800/height/320/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="320px" style="border:0px;"></iframe>
            #### Exerciseur 2 : Déterminer la nature d'une fonction avec sa représentation graphique
            <iframe scrolling="no" title="202 Déterminer la nature d'une fonction (graphiquement)" src="https://www.geogebra.org/material/iframe/id/r4k2dkcc/width/800/height/480/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="480px" style="border:0px;"></iframe>
            #### Exerciseur 3 : Identifier le coefficient directeur et l'ordonnée à l'origine (forme classique)
            <iframe scrolling="no" title="203 Coefficient directeur et ordonnée à l'origine (cas simples)" src="https://www.geogebra.org/material/iframe/id/kxtuj3rz/width/800/height/380/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="380px" style="border:0px;"></iframe>
            #### Exerciseur 4 : Identifier le coefficient directeur et l'ordonnée à l'origine (formes diverses)
            <iframe scrolling="no" title="204 de Coefficient directeur et ordonnée à l'origine (diverses formes)" src="https://www.geogebra.org/material/iframe/id/xta4exsm/width/800/height/410/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="410px" style="border:0px;"></iframe>
      
        === "Retrouver une expression algébrique<br>avec une représentation graphique"
            #### Exerciseur 1 : Déterminer l'ordonnée à l'origine
            <iframe scrolling="no" title="301 Déterminer graphiquement l'ordonnée à l'origine" src="https://www.geogebra.org/material/iframe/id/bqjuq8e8/width/800/height/480/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="480px" style="border:0px;"></iframe>
            #### Exerciseur 2 : Déterminer graphiquement signe du coefficient directeur
            <iframe scrolling="no" title="302 Déterminer le signe du coefficient directeur" src="https://www.geogebra.org/material/iframe/id/bbxudgmq/width/800/height/480/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="480px" style="border:0px;"></iframe>
            #### Exerciseur 3 : Déterminer graphiquement le coefficient directeur (fonction linéaire)
            <iframe scrolling="no" title="303 Déterminer graphiquement le coefficient directeur (fonction linéaire)" src="https://www.geogebra.org/material/iframe/id/n4bauamy/width/800/height/520/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="520px" style="border:0px;"></iframe>
            #### Exerciseur 4 : Déterminer graphiquement le coefficient directeur
            <iframe scrolling="no" title="304 Déterminer graphiquement le coefficient directeur" src="https://www.geogebra.org/material/iframe/id/dvpkgrda/width/800/height/520/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="520px" style="border:0px;"></iframe>
            #### Exerciseur 5 : Associer une fonction linéaire à sa représentation graphique
            <iframe scrolling="no" title="305 Associer une fonction linéaire à sa droite représentative" src="https://www.geogebra.org/material/iframe/id/qrdvakpn/width/800/height/520/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="520px" style="border:0px;"></iframe>
            #### Exerciseur 6 : Associer une fonction affine à sa représentation graphique
            <iframe scrolling="no" title="307 Associer une fonction affine à sa droite représentative" src="https://www.geogebra.org/material/iframe/id/b23cqtgt/width/800/height/520/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="520px" style="border:0px;"></iframe>
            #### Exerciseur 7 : Déterminer graphiquement l'expression algébrique d'une fonction affine
            <iframe scrolling="no" title="306 Déterminer graphiquement l'expression algébrique d'une fonction affine" src="https://www.geogebra.org/material/iframe/id/fcwmwqtp/width/800/height/520/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="520px" style="border:0px;"></iframe>
        
        === "Retrouver une expression<br>algébrique par le calcul"
            #### Exerciseur 1 : Retrouver par le calcul le coefficient directeur d'une fonction affine (cas où ce coefficient est entier)
            <iframe scrolling="no" title="F21 Calculer un coefficient directeur entier" src="https://www.geogebra.org/material/iframe/id/myvaeb9k/width/770/height/350/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="770px" height="350px" style="border:0px;"></iframe>
            #### Exerciseur 2 : Retrouver par le calcul le coefficient directeur d'une fonction affine
            <iframe scrolling="no" title="F22 Calculer un coefficient directeur" src="https://www.geogebra.org/material/iframe/id/rwffbak9/width/770/height/350/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="770px" height="350px" style="border:0px;"></iframe>
            #### Exerciseur 3 : Déterminer par le calcul l'expression algébrique d'une fonction affine (cas où les coefficient sont entiers)
            <iframe scrolling="no" title="F23 Retrouver une fonction affine avec 2 points de sa représentation (coeffs entiers)" src="https://www.geogebra.org/material/iframe/id/jdwj5gue/width/765/height/780/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="765px" height="780px" style="border:0px;"></iframe>
            #### Exerciseur 4 : Déterminer par le calcul l'expression algébrique d'une fonction affine
            <iframe scrolling="no" title="F24 Retrouver une fonction affine avec 2 points de sa représentation" src="https://www.geogebra.org/material/iframe/id/rqfuncbw/width/765/height/900/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="765px" height="900px" style="border:0px;"></iframe>

        === "Sens de variation et signe<br>d'une fonction affine"
            #### Exerciseur 1 : Déterminer le sens de variation d'une fonction affine (niveau 1)
            <iframe scrolling="no" title="F25 Sens de variation (niveau 1)" src="https://www.geogebra.org/material/iframe/id/gsvamat4/width/800/height/495/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="495px" style="border:0px;"></iframe>
            #### Exerciseur 2 : Déterminer le sens de variation d'une fonction affine (niveau 2)
            <iframe scrolling="no" title="F26 Sens de variation (niveau 2)" src="https://www.geogebra.org/material/iframe/id/y4sx7xrc/width/800/height/495/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="495px" style="border:0px;"></iframe>
            #### Exerciseur 3 : Déterminer le signe d'une fonction affine (niveau 1)
            <iframe scrolling="no" title="F27 Déterminer le signe d'une fonction affine (niveau 1)" src="https://www.geogebra.org/material/iframe/id/qtv5e4at/width/799/height/820/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="799px" height="820px" style="border:0px;"></iframe>
            #### Exerciseur 4 : Déterminer le signe d'une fonction affine (niveau 2)
            <iframe scrolling="no" title="F28 Déterminer le signe d'une fonction affine (niveau 2)" src="https://www.geogebra.org/material/iframe/id/rvkpefhz/width/800/height/820/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="820px" style="border:0px;"></iframe>
            #### Exerciseur 5 : Déterminer le signe d'une fonction affine (niveau 3)
            <iframe scrolling="no" title="F29 Déterminer le signe d'une fonction affine (niveau 3)" src="https://www.geogebra.org/material/iframe/id/upb6fajr/width/800/height/820/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="820px" style="border:0px;"></iframe>

        === "Représenter une<br>fonction affine"
            #### Exerciseur 1 : Représenter une fonction linéaire
            <iframe scrolling="no" title="401 Représenter une fonction linéaire" src="https://www.geogebra.org/material/iframe/id/umhkxpd7/width/800/height/520/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="520px" style="border:0px;"></iframe>
            #### Exerciseur 2 : Représenter une fonction affine (ordonnée à l'origine)
            <iframe scrolling="no" title="402 Représenter une fonction affine : ordonnée à l'origine" src="https://www.geogebra.org/material/iframe/id/mx6wtmuq/width/800/height/520/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="520px" style="border:0px;"></iframe>
            #### Exerciseur 3 : Représenter une fonction affine (coefficient directeur)
            <iframe scrolling="no" title="403 Représenter une fonction affine : coefficient directeur" src="https://www.geogebra.org/material/iframe/id/djabhk5n/width/800/height/520/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="520px" style="border:0px;"></iframe>
            #### Exerciseur 4 : Représenter une fonction affine
            <iframe scrolling="no" title="404 Représenter une fonction affine" src="https://www.geogebra.org/material/iframe/id/anwgpz2j/width/800/height/520/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="520px" style="border:0px;"></iframe>
