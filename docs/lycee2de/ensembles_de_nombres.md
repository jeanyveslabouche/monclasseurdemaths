title: Ensembles de nombres, intervalles et valeur absolue

!!! abstract "Des vidéos et des exerciseurs sont proposés sur cette page pour travailler ces notions."


!!! success "Les vidéos"
    | Les ensembles de nombres | Notion d'intervalles | Valeur absolue d'un nombre et distance entre deux réels |
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/UmhEYGbEO70?si=wGmTS2nW8JGQyXGU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/_0_GH4qt-cg?si=Dmus-yn2qjf3PMlz" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/A1bIc8B9KPM?si=uGc2AGTqRdXo_tvp" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|
    
    | Caractériser un intervalle avec une inégalité et une valeur absolue | Intersection de deux intervalles | Réunion de deux intervalles |
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/ZXrUOqj48UA?si=FZEXLxh3JY21tAAl" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/gM73tg88aW8?si=A3jxR6gJ2i_SX7vz" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/8G5n258mUxc?si=SnRqtIPTJy590v9B" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|


!!! success "Les exerciseurs"
    #### Exerciseur 1 : Traduire l'appartenance à un intervalle par une inégalité ou un encadrement
    <iframe scrolling="no" title="Traduire un intervalle en une inégalité ou un encadrement" src="https://www.geogebra.org/material/iframe/id/c4ymqrcf/width/800/height/480/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="800px" height="480px" style="border:0px;"> </iframe>
    #### Exerciseur 2 : Traduire une inégalité ou un encadrement par l'appartenance à un intervalle
    <iframe scrolling="no" title="I2 Traduire une inégalité ou un encadrement par une appartenance à un intervalle" src=
    "https://www.geogebra.org/material/iframe/id/gjkxzhzw/width/860/height/440/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"
    width="860px" height="440px" style="border:0px;"></iframe>
    #### Exerciseur 3 : Représenter un intervalle
    <iframe scrolling="no" title="I3 Représenter un intervalle" src=
    "https://www.geogebra.org/material/iframe/id/s6vtedp7/width/800/height/475/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"
    width="800px" height="475px" style="border:0px;"></iframe>
    #### Exerciseur 4 : Représenter un ensemble de nombres réels donné par une inégalité ou un encadrement
    <iframe scrolling="no" title="I4 Représenter un ensemble de réels (inégalité)" src=
    "https://www.geogebra.org/material/iframe/id/knbmpnsp/width/794/height/475/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"
    width="794px" height="475px" style="border:0px;"></iframe>
    #### Exerciseur 5 : Intersection de deux intervalles
    <iframe scrolling="no" title="Intersection de deux intervalles" src=
    "https://www.geogebra.org/material/iframe/id/tm99chrp/width/840/height/440/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"
    width="840px" height="440px" style="border:0px;"></iframe>
    #### Exerciseur 6 : Réunion de deux intervalles
    <iframe scrolling="no" title="I6 Réunion de deux intervalles" src=
    "https://www.geogebra.org/material/iframe/id/ddjjm2wq/width/920/height/490/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"
    width="920px" height="490px" style="border:0px;"></iframe>
    #### Exerciseur 7 : Représenter un intervalle défini par une inégalité (distance)
    <iframe scrolling="no" title="I7 Distance et représentation d'intervalles" src=
    "https://www.geogebra.org/material/iframe/id/rq4axy7q/width/800/height/472/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"
    width="800px" height="472px" style="border:0px;"></iframe>
    #### Exerciseur 8 : Distance et intervalle
    <iframe scrolling="no" title="I8 Distance et intervalle" src=
    "https://www.geogebra.org/material/iframe/id/asazn4v9/width/860/height/440/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"
    width="860px" height="440px" style="border:0px;"></iframe>




