title: Calculer avec les racines carrées

!!! abstract "Une série de vidéos (cours, démonstrations et exercices corrigés) pour apprendre ou réviser les méthodes de calcul avec les racines carrées. Une série d'exerciseurs pour s'entraîner"


!!! success "Les vidéos"
    | Une vidéo pour se familiariser avec les propriétés<br> de calcul avec les racines carrées| Une vidéo pour apprendre à écrire un quotient<br> sans radical au dénominateur| Une vidéo pour s’entraîner à calculer<br> en utilisant la quantité conjuguée|
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/IBDqCzIjea0?si=5qC8qhI1KdqEtB9D" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/RaSNrvgrgPg?si=ecQSoYL3BM7oCkam" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/BZBTv1RUlgo?si=VLrIwHCCRYCD5Ww-" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|

    |Démontrer que le la racine carrée d'un produit est égale<br>au produit des racines carrées|Démontrer que la racine carrée d'une somme est strictement <br>inférieure à la somme des racines carrées|Démontrer que racine carrée de 2 n'est pas un nombre rationnel|
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/m7vdhTqARqA?si=aSUnjWzqPkGHrbH9" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/lvq_d6OEzDs?si=1mZi7FqE1cqMv0LA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/xuijxhzmDgc?si=84tyzM8LJcoZhigS" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|

!!! success "Un exercice corrigé en vidéo"
    |Un exercice corrigé en vidéo : calcul avec les racines carrées|Consigne de l'exercice corrigé dans la vidéo ci-contre :|
    | :------------: | :-------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/D_Z7WoDoBHw?si=aUU7NKfDv90Md2bw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|![Exercice corrigé](images/exoracine.png)|


!!! success "Les exerciseurs"
    #### Exerciseur 1 : Avec la définition
    <iframe scrolling="no" title="01- Racine carrée d'un nombre" src="https://www.geogebra.org/material/iframe/id/h3skydxe/width/800/height/500/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="500px" style="border:0px;"></iframe>
    #### Exerciseur 2 : Racine carré du produit de deux carrés
    <iframe scrolling="no" title="02- Racine carrée d'un produit de carrés" src="https://www.geogebra.org/material/iframe/id/mgkjp76j/width/800/height/500/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="500px" style="border:0px;"></iframe>
    #### Exerciseur 3 : Sommes de racine carrées
    <iframe scrolling="no" title="3- Somme de racines carrées" src="https://www.geogebra.org/material/iframe/id/emxbapnj/width/800/height/360/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="360px" style="border:0px;"></iframe>
    #### Exerciseur 4 : Écrire le produit de deux racines carrées avec une seule racine carrée
    <iframe scrolling="no" title="03- Produits de racines carrées" src="https://www.geogebra.org/material/iframe/id/fyrrmgws/width/800/height/400/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="400px" style="border:0px;"></iframe>
    #### Exerciseur 5 : Écrire le produit d'un nombre par une racine carrée avec une seule racine carrée
    <iframe scrolling="no" title="04- Écrire un produit avec une unique racine carrée" src="https://www.geogebra.org/material/iframe/id/ckrx9jgq/width/800/height/400/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="400px" style="border:0px;"></iframe>
    #### Exerciseur 6 : Simplifier une racine carrée
    <iframe scrolling="no" title="5- Simplifier une racine carrée" src="https://www.geogebra.org/material/iframe/id/s4euj4ft/width/800/height/400/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="400px" style="border:0px;"></iframe>
    #### Exerciseur 7 : Simplifier une somme de racines carrées
    <iframe scrolling="no" title="07- Copie de Simplifier une somme de racines carrées" src="https://www.geogebra.org/material/iframe/id/dcxwefyu/width/800/height/400/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="400px" style="border:0px;"></iframe>
    #### Exerciseur 8 : Simplifier une somme de racines carrées (niveau 2)
    <iframe scrolling="no" title="08- Simplifier une somme de racines carrées (niveau 2)" src="https://www.geogebra.org/material/iframe/id/fqknjsnz/width/800/height/400/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="400px" style="border:0px;"></iframe>
    #### Exerciseur 9 : Écrire un quotient sans radical au numérateur
    <iframe scrolling="no" title="07- Écrire un quotient sans radical au dénominateur" src="https://www.geogebra.org/material/iframe/id/xax9phha/width/800/height/370/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="370px" style="border:0px;"></iframe>
    #### Exerciseur 10 : Écrire un quotient sans radical au numérateur (niveau 2)
    <iframe scrolling="no" title="08- Écrire un quotient sans radical au dénominateur (niveau 2)" src="https://www.geogebra.org/material/iframe/id/cspqpav3/width/800/height/580/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="580px" style="border:0px;"></iframe>