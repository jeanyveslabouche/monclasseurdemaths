title: Sens de variation d'une fonction

!!! abstract "Une série de 4 vidéos pour apprendre ou réviser cette notion."

!!! success "Les vidéos"
    |Une vidéo pour apprendre à déterminer et démontrer le sens de variation d'une fonction affine.|Exemples de démonstrations montrant le sens de variation d'une fonction affine.  |Étude des variations de la fonction carrée avec la démonstration de ces variations.|
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/ndOrKP0mh4U?si=2l-klUeBnZNsQIhd" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/-iprq9GEcl0?si=qmG0Wu3gQYaEn169" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/RftJgel3ClA?si=MDdkgdAgJWzIUuU9" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|
    
    |Étude des variations de la fonction inverse<br>avec la démonstration de ces variations.|||
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/IQHXgaUkPdI?si=1rBob3S89orvxgx5" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|||

