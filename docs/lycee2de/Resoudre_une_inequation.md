title: Résoudre une inéquation

!!! abstract "Des vidéos et des exerciseurs sont proposés sur cette page pour travailler ces notions."

!!! success "Les vidéos"
    |Calculer avec des inégalités : sommes|Calculer avec des inégalités : produits|Résoudre une inéquation du type $ax+b \ge 0$|
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/Rb6k9qd8If4?si=LiGqh_sJBWdjgG3I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/OudnN2G9Omw?si=oWdbTtP6qIA40T3-" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/YfohJYZeqhk?si=sF9GS9VhVtHX7VnU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|

    |Résoudre une inéquation du type $ax+b \ge cx+d$|||
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/1hjl_AZTM1Y?si=jp1BVathakSXvokB" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|||

!!! success "Les exerciseurs"
    #### Exerciseur 1 : Résoudre une inéquation du type $ax > b$
    <iframe scrolling="no" title="I1 Inéquation ax &gt; b" src= "https://www.geogebra.org/material/iframe/id/phaycmyd/width/700/height/470/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="700px" height="470px" style="border:0px;"></iframe>
    #### Exerciseur 2 : Résoudre une inéquation du type $ax + b > 0$
    <iframe scrolling="no" title="I2 Inéquation ax+b &gt; 0" src="https://www.geogebra.org/material/iframe/id/gtjdbxp3/width/700/height/595/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="700px" height="595px" style="border:0px;"></iframe>
    #### Exerciseur 3 : Résoudre une inéquation du type $ax+b > cx$
    <iframe scrolling="no" title="I3 Inéquation ax+b &gt; cx" src="https://www.geogebra.org/material/iframe/id/t3rdzpce/width/700/height/595/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="700px" height="595px" style="border:0px;"></iframe>
    #### Exerciseur 4 : Résoudre une inéquation du type $ax+b > cx+d$ (solutions entières)
    <iframe scrolling="no" title="I4 Inéquation ax+b &gt; cx+d solutions entières" src="https://www.geogebra.org/material/iframe/id/p8hcgacq/width/700/height/690/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="700px" height="690px" style="border:0px;"></iframe>
    #### Exerciseur 5 : Résoudre une inéquation du type $ax+b > cx+d$ (solutions non toujours entières)
    <iframe scrolling="no" title="I5 Inéquation ax+b &gt; cx+d (solutions non toujours entières)" src="https://www.geogebra.org/material/iframe/id/prwq9zxr/width/700/height/695/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="700px" height="695px" style="border:0px;"></iframe>
