title: Tableaux de signes

!!! abstract "Trois vidéos pour apprendre ou réviser cette notion."

!!! success "Les vidéos"
    |Déterminer le signe d'une fonction affine|Dresser le tableau de signe d'un produit<br>de la forme $(ax+b)(cx+d)$|Dresser le tableau de signe d'une<br>fonction homographique $\frac{ax+b}{cx+d}$|
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/QwBzzQw2bjs?si=IpeEnZfl3firn7ck" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/rYi05p_J3Ag?si=UEz-WRKuv5hii_5v" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/ndiKiJGtQcw?si=bO381VclWI4S-4w9" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|
