title: Les démonstrations de la classe de seconde

!!! abstract "Toutes les démonstrations au programme de seconde (nouveaux programmes lycée  2019) en vidéo. Regarder les vidéos en mode plein écran, ce sera bien plus lisible !"

!!! success "Nombres et calculs : manipuler les nombres réels"
    |Démontrer que $\sqrt{2}$ n'est pas un nombre rationnel|Démontrer que $\frac{1}{3}$ n'est pas un nombre décimal|Pour mieux comprendre les deux démonstrations précédentes.|
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/xuijxhzmDgc?si=BNiwDhUnIh5eJYNI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/PM7zVSUUqKE?si=nNCmq20Pqsur9ydI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/UmhEYGbEO70?si=pYjjEphn4GTVGfB4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|

    |Démontrer que $\frac{1}{7}$ n'est pas un nombre décimal : on peut démontrer de même que $\frac{1}{3}$ n'est pas décimal (ou tout inverse de nombre premier autre que 2 et 5)|||
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/JluQtKBy9_M?si=7lmD4QKcAnFV6TLF" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|||

!!! success "Nombres et calculs : utiliser les notions de multiple, diviseur et de nombre premier"
     |Démontrer que si deux nombres $b$ et $c$ sont des multiples de $a$ alors leur somme $a+b$ est également un multiple de $a$|Démontrer que le carré d'un nombre impair est impair||
     | :------------: | :-------------: | :------------: |
     |<iframe width="400" height="225" src="https://www.youtube.com/embed/1JACvHRbGmA?si=_A1xOEvaFuv070Na" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/knp-GNc25wI?si=oMUqgz4i9FizkU56" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>||

!!! success "Nombres et calculs : Utiliser le calcul littéral"
    |Démontrer que la racine carrée d'une somme est strictement<br>inférieure à la somme des racines carrées|Démontrer que le la racine carrée d'un produit est<br> égale au produit des racines carrées|Illustration géométrique de l’égalité<br> $(a + b)² = a² + 2ab + b²$|
    | :------------: | :-------------: | :------------: |
     |<iframe width="400" height="225" src="https://www.youtube.com/embed/lvq_d6OEzDs?si=rkhsQnGMIIACggd1" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/m7vdhTqARqA?si=Q2wLaM5iD68jOyAH" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/XHbNVBxDbEU?si=wu_kDR-5IC_wF2LM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|

!!! success "Géométrie : Manipuler les vecteurs du plan"
    |Démontrer que deux vecteurs sont colinéaires si, et seulement si, leur déterminant est nul.|||
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/pJCgOvSSrys?si=EUWopITVq5W5GaqI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|||

!!! success "Géométrie : Résoudre des problèmes de géométrie"
    |Démontrer que le projeté orthogonal du point A sur une droite (Δ) est le point de la droite (Δ) le plus proche du point A.|Relation trigonométrique cos²(α) + sin²(α) = 1 dans un triangle rectangle||
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/BDRHhmKGSis?si=YvSgIqUElU-kA_qt" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/faaPNmO5QpQ?si=SVh-qG0mEF-K9A0x" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>||

!!! success "Géométrie : Représenter et caractériser les droites du plan"
    |Établir la forme générale d'une équation de droite en utilisant le déterminant|||
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/vypQw6Y7TvA?si=c36-kNfcaNH5DrSj" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|||

!!! success "Fonctions : Se constituer un répertoire de fonctions de référence"
    |Etude de la position relative de la droite d'équation y=x et des courbes représentatives des fonctions carrée et cube|||
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/_QK6ga3qIv8?si=zNsLB4NDVh_K2IM8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|||

!!! success "Fonctions : étudier les variations et les extremums d’une fonction"
    |Démontrer les variations de la fonction carrée|Démontrer les variations de la fonction inverse|Démontrer les variations de la fonction racine carrée|
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/RftJgel3ClA?si=HzpROHt-8dnyx9Nt" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/IQHXgaUkPdI?si=0zrg5UtiSTbnyx-i" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/oIOyoFPeI8k?si=RaoFX25hZAdND-_S" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|

