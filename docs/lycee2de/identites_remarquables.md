title: Les identités remarquables

!!! abstract "Des vidéos et des exerciseurs sont proposés sur cette page pour travailler ces notions."

!!! success "Les vidéos"
    | Comprendre l'identité remarquable $(a+b)(a-b)=a^2-b^2$ | Démonstration visuelle de l'identité remarquable $(a+b)(a-b)=a^2-b^2$ | Calculer mentalement avec l'identité remarquable $(a+b)(a-b)=a^2-b^2$|
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/YEJykVUDMMU?si=vHsQ_p1lFrGj17LY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/K_90_ixEU5k?si=WCwiLY8t0GipWCko" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/Jauk9Ha4-v0?si=WLVvTR2hHwqSUKpv" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|
    
    | Développer avec l'identité remarquable $(a+b)(a-b)=a^2-b^2$ | Factoriser avec l'identité remarquable $(a+b)(a-b)=a^2-b^2$ ||
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/QkLEFSIfhVI?si=V9Pk8R27aGTw39pc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/qFs0vfH50oE?si=swz6nuCPd3VOSt34" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>||

    |Comprendre les identités remarquables $(a+b)^2=a^2+2ab+b^2$ et $(a-b)^2=a^2-2ab+b^2$|Démonstration visuelle de l'identité remarquable $(a+b)^2=a^2+2ab+b^2$|Calculer mentalement avec les identités remarquables|
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/fUpZ0vhnyW0?si=irajSJfFOMZwEksw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/XHbNVBxDbEU?si=kYooIxISUr0eIU6V" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/UXkxNutKzks?si=jG9v40Y0VQY_TSwK" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|

    |Développer avec les identités remarquables $(a+b)^2=a^2+2ab+b^2$ et $(a-b)^2=a^2-2ab+b^2$|Factoriser avec les identités remarquables $(a+b)^2=a^2+2ab+b^2$ et $(a-b)^2=a^2-2ab+b^2$||
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/qhCLuqUNZ2c?si=pGoIjP5KHKqB8Jn9" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/QvfP6V2urQs?si=_9rhIpZpfaKIPS5I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>||

!!! success "Les exerciseurs"
    Ils sont répartis dans les trois onglets ci-dessous :

    === "Identité remarquable $a^2-b^2$"
        #### Exerciseur 1 : Développer une expression littérale (niveau 1)
        <iframe scrolling="no" title="ir3 Développer (niveau 1)" src="https://www.geogebra.org/material/iframe/id/mmzkyv2p/width/730/height/235/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="730px" height="235px" style="border:0px;"></iframe>
        #### Exerciseur 2 : Développer une expression littérale (niveau 2)
        <iframe scrolling="no" title="ir4 Développer (niveau 2)" src="https://www.geogebra.org/material/iframe/id/jbgwkrdv/width/730/height/240/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="730px" height="240px" style="border:0px;"></iframe>
        #### Exerciseur 3 : Factoriser une expression littérale (niveau 1)
        <iframe scrolling="no" title="ir5 Factoriser (niveau 1)" src="https://www.geogebra.org/material/iframe/id/tyts5ygu/width/730/height/250/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="730px" height="250px" style="border:0px;"></iframe>
        #### Exerciseur 4 : Factoriser une expression littérale (niveau 2)
        <iframe scrolling="no" title="ir6 Factoriser (niveau 2)" src="https://www.geogebra.org/material/iframe/id/eypeqx4t/width/730/height/244/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="730px" height="244px" style="border:0px;"></iframe>

    === "Identités remarquables $(a+b)^2$ et $(a-b)^2$"
        #### Exerciseur 1 : Développer une expression littérale (niveau 1)
        <iframe scrolling="no" title="id1 Développer avec identités (a±b)² (niveau 1)" src="https://www.geogebra.org/material/iframe/id/bxjwjdzg/width/730/height/240/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="730px" height="240px" style="border:0px;"></iframe>
        #### Exerciseur 2 : Développer une expression littérale (niveau 2)
        <iframe scrolling="no" title="id2 Développer avec identités (a±b)² (niveau 2)" src="https://www.geogebra.org/material/iframe/id/njmsxaja/width/730/height/240/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="730px" height="240px" style="border:0px;"></iframe>
        #### Exerciseur 3 : Factoriser une expression littérale (niveau 1)
        <iframe scrolling="no" title="id3 Factoriser avec identités (a±b)² (niveau 1)" src="https://www.geogebra.org/material/iframe/id/yfut95a5/width/730/height/320/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="730px" height="320px" style="border:0px;"></iframe>
        #### Exerciseur 4 : Factoriser une expression littérale (niveau 2)
        <iframe scrolling="no" title="id4 Factoriser avec identités (a±b)² (niveau 2)" src="https://www.geogebra.org/material/iframe/id/wy7ebhr5/width/730/height/355/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="730px" height="355px" style="border:0px;"></iframe>
        #### Exerciseur 5 : Factoriser une expression littérale (niveau 3)
        <iframe scrolling="no" title="id5 Factoriser avec identités (a±b)² (niveau 3)" src="https://www.geogebra.org/material/iframe/id/qtrvtvex/width/730/height/355/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="730px" height="355px" style="border:0px;"></iframe>

    === "Calcul mental avec les identités remarquables"
        #### Exerciseur 1 : Calculer mentalement une différence de carrés
        <iframe scrolling="no" title="ir1 Calcul mental (différence)" src="https://www.geogebra.org/material/iframe/id/wvcn5qbd/width/730/height/335/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="730px" height="335px" style="border:0px;"></iframe>
        #### Exerciseur 2 : Calculer mentalement un produit
        <iframe scrolling="no" title="ir2 Calcul mental (produit)" src="https://www.geogebra.org/material/iframe/id/asjrfnnm/width/730/height/285/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="730px" height="285px" style="border:0px;"></iframe>
        #### Exerciseur 3 : Calculer mentalement un carré
        <iframe scrolling="no" title="id6 Calcul mental (carré)" src="https://www.geogebra.org/material/iframe/id/fmhvfvw8/width/730/height/350/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false" width="730px" height="350px" style="border:0px;"></iframe>