title: Déterminer graphiquement la racine carrée d'un nombre

!!! abstract "Une méthode expliquée et démontrée en vidéo."

!!! success "La méthode expliquée en vidéo"
    |Cette vidéo explique étape par étape la construction. Puis la démonstration du résultat est également détaillée. |||
    | :------------: | :-------------: | :------------: |
    |<iframe width="560" height="315" src="https://www.youtube.com/embed/2oKUCH9e6bo?si=3sgrxS40gBhhNNt9" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|||

