title: Résoudre un système de deux équations à deux inconnues

!!! abstract "Une série de 4 vidéos pour apprendre ou réviser les méthodes de calcul. Une série d'exerciseurs pour s'entraîner."

!!! success "Les vidéos"
    |Combien de solutions possède un système<br> de 2 équations à 2 inconnues ?|Résoudre graphiquement un système<br> de 2 équations à 2 inconnues|Résoudre un système de 2 équations à 2 inconnues<br><span style="color:green">résolution par substitution</span>|
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/XGa3oulJ_P4?si=NncX47Ky-_l_EFqv" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/zz4_Vvw1Iqs?si=bIONZHWBANkIwhnb" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/RIo-Hhrf_34?si=oCOi7jci3TyVTqZF" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|

    |Résoudre un système de 2 équations à 2 inconnues<br><span style="color:green">résolution par conbinaison</span>|||
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/h8XX_Xp3KSM?si=-89O9K_QQ3BnENFG" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|||

!!! success "Les exerciseurs"
    #### Exerciseur 1 : Combien de solutions ?
    <iframe scrolling="no" title="01 Combien de solutions ?" src="https://www.geogebra.org/material/iframe/id/f6a3vydz/width/750/height/500/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="750px" height="500px" style="border:0px;"></iframe>
    #### Exerciseur 2 : Résolution graphique
    <iframe scrolling="no" title="08 Résolution graphique" src="https://www.geogebra.org/material/iframe/id/ajfhtgjs/width/760/height/720/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="760px" height="720px" style="border:0px;"></iframe>
    #### Exerciseur 3 : Un des coefficient vaut 1 (méthode par substitution)
    <iframe scrolling="no" title="02 Systèmes d'équations 1 coeff égal à 1 et solution entières" src="https://www.geogebra.org/material/iframe/id/fnhq5a4y/width/750/height/260/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="750px" height="260px" style="border:0px;"></iframe>
    #### Exerciseur 4 : Inconnues et coefficients entiers
    <iframe scrolling="no" title="03 Solutions entières" src="https://www.geogebra.org/material/iframe/id/bxm3exe3/width/760/height/260/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="760px" height="260px" style="border:0px;"></iframe>
    #### Exerciseur 5 : Une des deux inconnues est fractionnaire (coeffs entiers)
    <iframe scrolling="no" title="04 Une inconnue fractionnaire (coefficients entiers)" src="https://www.geogebra.org/material/iframe/id/esezenge/width/760/height/300/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="760px" height="300px" style="border:0px;"></iframe>
    #### Exerciseur 6 : Les deux inconnues sont fractionnaires (coeffs entiers)
    <iframe scrolling="no" title="05 2 inconnues fractionnaires (coefficients entiers)" src="https://www.geogebra.org/material/iframe/id/hcmk78yz/width/760/height/300/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="760px" height="300px" style="border:0px;"></iframe>
    #### Exerciseur 7 : Deux des coefficients et les inconnues sont fractionnaires
    <iframe scrolling="no" title="06 Deux coefficients fractionnaires (inconnues fractionnaires)" src="https://www.geogebra.org/material/iframe/id/gacykwfg/width/760/height/390/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="760px" height="390px" style="border:0px;"></iframe>
    #### Exerciseur 8 : Les coefficients et les inconnues sont fractionnaires
    <iframe scrolling="no" title="07 Coefficients fractionnaires" src="https://www.geogebra.org/material/iframe/id/wbp3e2hy/width/760/height/400/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="760px" height="400px" style="border:0px;"></iframe>