title: Coordonnées de points et configurations du plan

!!! abstract "Une série de 3 vidéos pour apprendre ou réviser les méthodes de calcul ainsi que des exercices corrigés en vidéos. Une série d'exerciseurs pour s'entraîner."

!!! success "Les vidéos"
    |Une vidéo pour apprendre à connaître<br> les différentes types de repères du plan|Une vidéo pour apprendre à calculer<br> la distance entre deux points du plan|Une vidéo pour apprendre à calculer<br> les coordonnées du milieu d'un segment|
    | :------------: | :-------------: | :------------: |
    |<iframe width="400" height="225" src="https://www.youtube.com/embed/-OElDZKHD-M?si=4Cycf7s64H-WlbEM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/q97EgULdA7Y?si=-mwUh5KPN6X7DF0N" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|<iframe width="400" height="225" src="https://www.youtube.com/embed/06njeY14ouc?si=iT43j_6JyoGhkG5A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|



!!! success "Des exercices corrigés en vidéos"
    |Les consignes des exercices|Les corrections en vidéo|
    | :------------: | :-------------: |
    |![Exercice corrigé](images/exopoint1.png)|<iframe width="400" height="225" src="https://www.youtube.com/embed/J5tiIK3my4o?si=2ZDDswDVklHvOuVk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|
    |![Exercice corrigé](images/exopoint2.png)|<iframe width="400" height="225" src="https://www.youtube.com/embed/-8FsvpCm1DM?si=XM7zYgUOVZHEl1Pc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|
    |![Exercice corrigé](images/exopoint3.png)|<iframe width="400" height="225" src="https://www.youtube.com/embed/1hOxtfoa1Vw?si=7NSW6ge2QUgb2uO5" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|
    |![Exercice corrigé](images/exopoint4.png)|<iframe width="400" height="225" src="https://www.youtube.com/embed/Jnt1lCI7Mgs?si=MBg0Q8HMuDDgXCwo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|
    |![Exercice corrigé](images/exopoint5.png)|<iframe width="400" height="225" src="https://www.youtube.com/embed/C0YiXaNpxH8?si=k3iGdhL7ue3qjvFl" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>|


!!! success "Les exerciseurs"
    #### Exerciseur 1 : Calculer la distance entre deux points
    <iframe scrolling="no" title="Calculer la distance entre deux points" src="https://www.geogebra.org/material/iframe/id/fyv8saav/width/800/height/460/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="460px" style="border:0px;"></iframe>
        #### Exerciseur 2 : Calculer les coordonnées du milieu d'un segment
    <iframe scrolling="no" title="Coordonnées du milieu d'un segment" src="https://www.geogebra.org/material/iframe/id/fbsubdxk/width/800/height/600/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="600px" style="border:0px;"></iframe>
    #### Exerciseur 3 : Compléter un parallélogramme (déterminer les coordonnées du 4e sommet)
    <iframe scrolling="no" title="Compléter le parallélogramme" src="https://www.geogebra.org/material/iframe/id/udqgqbqp/width/800/height/600/border/888888/sfsb/true/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/false/rc/false/ld/false/sdz/false/ctl/false"width="800px" height="600px" style="border:0px;"></iframe>
