---
author: Jean-Yves Labouche
title: 🏡 Accueil
---

# Mon Classeur de Maths

!!! info "Migration du site monclasseurdemaths.fr"

    Ce site est une tentative de migration de mon [site](https://www.monclasseurdemaths.fr/) sur la forge des communs numériques éducatifs.
    Cela va prendre beaucoup de temps !!! Le site d'origine reste disponible et il est mis à jour en priorité tant que cette version ne sera pas complète.

## Ressources pour les lycéens

[![2de](images\classe_de_2de.png){ width=20% }](lycee2de/sommaire_seconde/) [![1re](images\classe_de_1re.png){ width=20% }](lycee_1re/sommaire_premiere/) [![Tle](images\classe_de_tle.png){ width=20% }](lycee_tle/sommaire_terminale/)


## Ressources pour les collégiens



!!! abstract "Avancement du projet"
    **Partie du site pour les lycéens**
    :white_check_mark: Classe de seconde<br>
    :white_check_mark: Classe de première<br>
    :x: Classe de terminale

    **Partie du site pour les collégiens**
    :x: Classe de 6e <br>
    :x: Cycle 4 <br>
    :x: Tutoriels 
    **Parties du site pour les enseignants**
    :x:  Lycée <br>
    :x:  Collège <br>
    :x:  Fablab
    

